import org.openqa.selenium.*;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

class test_scenarios {

    static void login_test() throws SQLException {

        WebDriver driver = utils.init_driver();

        Counters.date = LocalDateTime.now();

        Counters.start_date = Counters.date.format(DateTimeFormatter.ofPattern("uuuu.MM.dd-HH.mm.ss"));
        Counters.timestamp = Counters.date.format(DateTimeFormatter.ofPattern("uuuuMMddHHmmss"));

//        Counters.base_url = "https://onytrex.com/";
        Counters.base_url = getConfig.get("baseUrl");

        try{
            driver.get(Counters.base_url);
            driver.manage().window().setSize(new Dimension(1600, 900));
        } catch (Exception e)
        {
            e.printStackTrace();
        }
//        Counters.email = "@tugush.st";
//        Counters.password = "1234567890";
        Counters.password = getConfig.get("password");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Counters.email = getConfig.get("email_start") + Counters.timestamp + getConfig.get("email_tail");
//        Counters.email = "autotests+" + Counters.timestamp + Counters.email;

        WebElement email_field = null;
        WebElement pass_field = null;



//
//        try {
//            utils.pass_out(driver, Counters.step);
//        } catch (Exception e) {
//            utils.error_out(e,driver, Counters.step);
//        }



//      CLICK OPEN ACCOUNT
//        try {
//            Counters.step = "click_login";
//            WebElement login_a=driver.findElement(By.xpath("//*[contains(text(), 'Open account')]"));
//            login_a.click();
//            utils.pass_out(driver, Counters.step);
//        } catch (Exception e) {
//            utils.error_out(e,driver, Counters.step);
//        }

        try {

            Counters.step = "click_login";
            WebElement sing_up_button = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Log In'])[2]/following::div[2]"));
            sing_up_button.click();
            utils.pass_out(driver, Counters.step);
        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {


            Counters.step = "fill_email";
//            WebElement email_field=driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Already registered? Sign In'])[1]/following::input[1]"));
            email_field=driver.findElement(By.id("email"));
            email_field.click();
            email_field.sendKeys("EEEEE");
            utils.pass_out(driver, Counters.step);
        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {

            Counters.step = "fill_pass";
//            WebElement pass_field=driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Already registered? Sign In'])[1]/following::input[2]"));
            pass_field=driver.findElement(By.id("password"));
            pass_field.click();
            pass_field.sendKeys("EEEEE");
            utils.pass_out(driver, Counters.step);

        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {

            Counters.step = "click_create";
            assert pass_field != null;
            pass_field.click();
            pass_field.sendKeys(Keys.ENTER);
            utils.pass_out(driver, Counters.step);


        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {



            Counters.step = "check_error_present"; // E-mail is not correct
            WebElement error_message = driver.findElement(By.xpath("//*[contains(text(), 'E-mail is not correct')]"));
            error_message.click();
            utils.pass_out(driver, Counters.step);
        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {

            Counters.step = "Correct_email"; // E-mail is not correct at the moment
            assert email_field != null;
            email_field.click();
            email_field.clear();
            email_field.sendKeys(Counters.email);
            pass_field.sendKeys(Keys.ENTER);
            utils.pass_out(driver, Counters.step);
        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {


            Counters.step = "check_password_error"; // E-mail is correct
            WebElement error_message_2 = driver.findElement(By.xpath("//*[contains(text(), 'Min password length - 9 symbols.')]"));
            error_message_2.click();
            utils.pass_out(driver, Counters.step);
        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {

            Counters.step = "check_user_create"; // E-mail is correct, pass_steps is correct
            pass_field.clear();
            pass_field.sendKeys(Counters.password);
            pass_field.sendKeys(Keys.ENTER);
            utils.pass_out(driver, Counters.step);
        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {

            Counters.step = "check_user_login";
            String curr_url = driver.getCurrentUrl();
            String should_be_url = Counters.base_url + "trading-view/BTC-USD";
            if (curr_url.equals(should_be_url)) {
                utils.pass_out(driver, Counters.step);
            } else {
                Exception e = new Exception("No BTC-USD on login");
                utils.error_out(e, driver, Counters.step);
            }
        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {
            Counters.step = "check_logout_button";
            WebElement logout_button = driver.findElement(By.id("logout"));
            utils.pass_out(driver, Counters.step);
            Counters.step = "logout";
            logout_button.click();

        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {

            Counters.step = "login";
//            driver.manage().deleteAllCookies();
            driver.get(Counters.base_url);
            Thread.sleep(2000);
            WebElement login_button = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Exchange'])[2]/following::div[4]"));
            login_button.click();
            email_field=driver.findElement(By.id("email"));
            pass_field=driver.findElement(By.id("password"));
            email_field.sendKeys(Counters.email);
            pass_field.sendKeys(Counters.password);
            pass_field.sendKeys(Keys.ENTER);
            utils.pass_out(driver, Counters.step);

        } catch (Exception e) {
            utils.error_out(e, driver, Counters.step);

        }

        try {

            Counters.step = "check_login_success";
            Thread.sleep(5000);
            if (driver.getCurrentUrl().equals(Counters.base_url + "trading-view/BTC-USD")) {
                utils.pass_out(driver, Counters.step);
            } else {
                Exception e = new Exception("No BTC-USD on login");
                utils.error_out(e, driver, Counters.step);
            }


        } catch (Exception e) {
            utils.error_out(e,driver, Counters.step);
        }


        utils.final_out();
        driver.close();

    }

}
