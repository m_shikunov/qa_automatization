

import java.sql.*;

class dbConn {


    public static Connection connect () {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://tugush.cloctommwngl.eu-central-1.rds.amazonaws.com:5432/qa_internal?prepareThreshold=0",
                            "automata", "Pa$$W0rd");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
//        System.out.println("Opened database successfully");
        return c;
    }
//
//    public static int select_try (int issue) throws SQLException {
//        Connection c = connect();
//        int tr=0;
//        String query = "SELECT try FROM issue_notify WHERE issue=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setInt(1, issue);
//            ResultSet rs = st.executeQuery();
//            while ( rs.next()){
//                tr = rs.getInt("try");
//            }
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            tr = -1;
//        }
//        c.close();
//        return tr;
//
//    }
//
//
//    public static long select_time (int issue) throws SQLException {
//        Connection c = connect();
//        long tr=0;
//        String query = "SELECT timestamp FROM issue_notify WHERE issue=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setInt(1, issue);
//            ResultSet rs = st.executeQuery();
//            while ( rs.next()){
//                tr = rs.getLong("timestamp");
//            }
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            tr = -1;
//        }
//        c.close();
//        return tr;
//
//    }
//
//    public static boolean select_stop (int issue) throws SQLException {
//        Connection c = connect();
//        boolean tr = false;
//        String query = "SELECT stop_notify FROM issue_notify WHERE issue=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setInt(1, issue);
//            ResultSet rs = st.executeQuery();
//            while ( rs.next()){
//                tr = rs.getBoolean("stop_notify");
//            }
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            tr = false;
//        }
//        c.close();
//        return tr;
//
//    }
//
//    public static boolean select_was_tested (int issue) throws SQLException {
//        Connection c = connect();
//        boolean tr = false;
//        String query = "SELECT was_testing FROM issue_notify WHERE issue=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setInt(1, issue);
//            ResultSet rs = st.executeQuery();
//            while ( rs.next()){
//                tr = rs.getBoolean("was_testing");
//            }
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            tr = false;
//        }
//        c.close();
//        return tr;
//
//
//    }
//
//    public static void update_issue_notify (int issue, int tr, String status) throws SQLException {
//        Connection c = connect();
//        String query = "UPDATE issue_notify SET timestamp="+"?"+", try="+"?"+", last_status="+"?"+" WHERE issue=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setLong(1, System.currentTimeMillis());
//            st.setInt(2, tr);
//            st.setString(3, status);
//            st.setInt(4, issue);
//            st.executeUpdate();
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        c.close();
//
//
//    }
//
//    public static void update_issue_notify_2 (int issue, String qa, String status, boolean was_t,  boolean stop) throws SQLException {
//        Connection c = connect();
//        String query = "UPDATE issue_notify SET qa="+"?"+", timestamp="+"?"+", last_status="+"?"+", was_testing="+"?" +", stop_notify="+"?"+" WHERE issue=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setString(1, qa);
//            st.setLong(2, System.currentTimeMillis());
//
//            st.setString(3, status);
//            st.setBoolean(4, was_t);
//            st.setBoolean(5, stop);
//            st.setInt(6, issue);
//            st.executeUpdate();
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        c.close();
//
//
//    }
//
//    public static void update_issue_notify_3 (int issue, String status, boolean was_t,  boolean stop) throws SQLException {
//        Connection c = connect();
//        String query = "UPDATE issue_notify SET timestamp="+"?"+", last_status="+"?"+", was_testing="+"?" +", stop_notify="+"?"+" WHERE issue=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//
//            st.setLong(1, System.currentTimeMillis());
//
//            st.setString(2, status);
//            st.setBoolean(3, was_t);
//            st.setBoolean(4, stop);
//            st.setInt(5, issue);
//            st.executeUpdate();
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        c.close();
//
//
//    }
//
//    public static void create_issue_notify (int issue, String qa, int tr, String status, boolean was_test, boolean stop) throws SQLException {
//        Connection c = connect();
//        String query = "INSERT INTO issue_notify (qa, timestamp, try, last_status, was_testing, stop_notify, issue) VALUES (" +
//                "?, ?, ?, ?, ?, ?, ?)";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setString(1, qa);
//            st.setLong(2, System.currentTimeMillis());
//            st.setInt(3, tr);
//            st.setString(4, status);
//            st.setBoolean(5, was_test);
//            st.setBoolean(6, stop);
//            st.setInt(7, issue);
//            st.executeUpdate();
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        c.close();
//
//
//
//    }

//
//    public static int select_status(Connection c, String env, String proj) {
//
//        int status = 1;
//        String proj_true = env + "_" + proj;
//
//        String query = "SELECT status FROM dev_status WHERE service=" + "?" ;
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setString(1, proj_true);
//            ResultSet rs = st.executeQuery();
//            while ( rs.next()){
//                status = rs.getInt("status");
//            }
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            status = 2;
//        }
//
//
//        return status;
//    }
//
//    public static void set_status (Connection c, String proj, String time, int status, String env) {
//        String proj_true = env + "_" + proj;
//        String query = "UPDATE dev_status SET status="+"?"+", time_start="+"?"+" WHERE service=" + "?";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setInt(1, status);
//            st.setString(2, time);
//            st.setString(3, proj_true);
//            st.executeUpdate();
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    public static int insertInterval(Connection c, String product, String type, String error, String start_date, String end_date, int idle_time ){
//        int id=0;
//        String query = "INSERT INTO dev_mon ("
//                + " product,"
//                + " type,"
//                + " error,"
//                + " start_date,"
//                + " end_date,"
//                + " idle_time) VALUES ("
//                + "?, ?, ?, ?, ?, ?)";
//        try {
//            PreparedStatement st = c.prepareStatement(query);
//            st.setString(1, product);
//            st.setString(2, type);
//            st.setString(3, error);
//            st.setString(4, start_date);
//            st.setString(5, end_date);
//            st.setInt(6, idle_time);
//
//            st.executeUpdate();
//            st.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        String query2 = ("SELECT id FROM dev_mon WHERE start_date='" + start_date + "' AND end_date='" + end_date + "'" );
//        try{
//            PreparedStatement ts = c.prepareStatement(query2);
//            ResultSet rs = ts.executeQuery();
//            while ( rs.next()){
//                id = rs.getInt("id");
//            }
//            ts.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return id;
//    }
//
//    public static void closeConn(Connection c) throws SQLException {
//        c.close();
//    }


}