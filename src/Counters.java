import java.time.LocalDateTime;

class Counters {
    static int pass_steps = 0;
    static int fail_steps = 0;
    static String step = "step_init";
    static String pass_string;
    static String fail_string;
    static LocalDateTime date;
    static String start_date;
    static String email;
    static String password;
    static String timestamp;
    static String base_url;
}