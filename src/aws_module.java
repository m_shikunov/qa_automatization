import java.io.File;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

class aws_module {


    static void upload(String fileName, String folder, String filePath) {
        String clientRegion = "eu-central-1";
        String bucketName = "test.onytrex.com";
        String stringObjKeyName = folder + "/" +fileName;
//        String stringObjKeyName = fileName;
        String fileObjKeyName = folder + "/" +fileName;
//        String fileObjKeyName = fileName;

        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(clientRegion)
                    .withCredentials(new ProfileCredentialsProvider())
                    .build();

            // Upload a text string as a new object.
            s3Client.putObject(bucketName, stringObjKeyName, "Uploaded String Object");

            // Upload a file as a new object with ContentType and title specified.
            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, new File(filePath));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("image/png");
//            metadata.setContentType("text/html");
            request.setMetadata(metadata);
            s3Client.putObject(request);
        } catch(SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
    }
}