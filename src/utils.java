import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.*;

class utils {



    private static String screen(WebDriver driver, String step, Boolean fail) {
        LocalDateTime date = LocalDateTime.now();
        //        LocalTime time = LocalTime.now();
//        String b = time.format(DateTimeFormatter.BASIC_ISO_DATE);
        String number = date.format(DateTimeFormatter.ofPattern("uuuu.MM.dd-HH.mm.ss"));

        String screen_name;
        screen_name = Counters.start_date + "//" + number + "-" + step + "-screen" +  ".png";
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("Screenshots//"  + screen_name ));
//            FileUtils.copyFile(scrFile, new File("C://screenshots//autotests//"  + screen_name + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (fail) {
            aws_module.upload(step + ".png", "Screenshots/" + Counters.start_date, "Screenshots/" + screen_name);
            screen_name = "http://test.onytrex.com.s3-website.eu-central-1.amazonaws.com/Screenshots/"  + Counters.start_date + "/" + step + ".png";
        }




        return screen_name;
    }



    static void error_out(Exception e, WebDriver driver, String step){
        System.out.println("Step " + step +" Failed");
        String screen = utils.screen(driver,step,true);
        System.out.println(screen);
        System.out.println("Error--->  " + e);
        Counters.fail_steps = Counters.fail_steps +1;
        Counters.fail_string = Counters.fail_string + " -+- " + step + "failed screen: " + screen;

    }

    static void pass_out (WebDriver driver, String step)  {
        System.out.println("Step " + step +" Pass");
        System.out.println(utils.screen(driver,step,false));
        Counters.pass_steps = Counters.pass_steps +1;
        Counters.pass_string = Counters.pass_string + " -+- " + step;
    }


    static void final_out () {
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println("Steps passed == " + Counters.pass_steps);
        System.out.println("Steps failed == " + Counters.fail_steps);
        System.out.println("Failed steps are :: " + Counters.fail_string);
        System.out.println("email is " + Counters.email);
        System.out.println("password is " + Counters.password);
        String send_tg = "autotest%20finished" + "%20Steps%20passed%20" + Counters.pass_steps + "%20Failed_steps%20" + Counters.fail_steps + "%20%20%20" + "%20Failed%20steps%20are%20%20%20" + Counters.fail_string;
        System.out.println(send_tg);
        int code = send_tg_message(send_tg);
        System.out.println("Get response for tg bot is : " + code);

    }


    private static int send_tg_message(String text_to_send) {
        int code = 0;
        String response;

        try {

            String urlString = getConfig.get("tg_bot_send");

            String apiToken = getConfig.get("tg_bot_apikey");

            String chatId = getConfig.get("tg_bot_chat_id");


            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("177.36.6.250", 8080));


            urlString = String.format(urlString, apiToken, chatId, text_to_send);

            URL url = new URL(urlString);

            System.out.println(urlString);

            URLConnection conn = url.openConnection(proxy);

            StringBuilder sb = new StringBuilder();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            response = sb.toString();
            System.out.println(response);


        } catch (IOException | SQLException e) {
            e.printStackTrace();
            System.out.println("connect fail_steps 500");
            code = 500;
        }

        return code;

    }

    static WebDriver init_driver() {


//        System.setProperty("webdriver.chrome.driver", "chromedriver");
//        WebDriver driver = new ChromeDriver();

        DesiredCapabilities capability = DesiredCapabilities.firefox();
        capability.setBrowserName("firefox");
        String hubUrl = "http://54.93.103.197:4444/wd/hub";

        WebDriver driver = null;
            try {
                driver = new RemoteWebDriver(new URL(hubUrl), capability);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        return driver;


    }




}


